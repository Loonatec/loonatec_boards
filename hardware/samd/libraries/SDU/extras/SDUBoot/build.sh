#!/bin/sh -x

ARDUINO=arduino
SKETCH_NAME="SDUBoot.ino"
SKETCH="$PWD/$SKETCH_NAME"
BUILD_PATH="$PWD/build"
OUTPUT_PATH="../../src/boot"

if [[ "$OSTYPE" == "darwin"* ]]; then
	ARDUINO="/Applications/Arduino.app/Contents/MacOS/Arduino"
fi

buildSDUBootSketch() {
	BOARD=$1
	DESTINATION=$2

	$ARDUINO --verify --board $BOARD --preserve-temp-files --pref build.path="$BUILD_PATH" $SKETCH
	cat "$BUILD_PATH/$SKETCH_NAME.bin" | xxd -i > $DESTINATION
	rm -rf "$BUILD_PATH"
}

mkdir -p "$OUTPUT_PATH"

# Use case-sensitive names from boards.txt file
buildSDUBootSketch "loonatec:samd:hab_terminator_v1" "$OUTPUT_PATH/hab_terminator_v1.h"
buildSDUBootSketch "loonatec:samd:uas_bounder_v1" "$OUTPUT_PATH/uas_bounder_v1.h"

# Then update ../../src/SDU.cpp file to reflect new board options
#   Note: Look at boards.txt "xxx.build.extra_flags=-Dxxx" for correct #defined names
