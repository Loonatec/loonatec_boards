/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "variant.h"

#include "wiring_constants.h"
#include "wiring_digital.h"
#include "nrf.h"

const uint32_t g_ADigitalPinMap[] = {
  // D0 - D7
  0,  // xtal 1
  1,  // xtal 2
  2,  // PGOOD
  3,  // MTR_IN1
  4,  // MTR_IN2
  5,  // a3 - Batt_Volt
  6,  // TXD
  7,  // MTR_EN

  // D8 - D13
  8,  // RXD

  9,  // NFC1 - NC
  10, // NFC2 - NC

  11, // POS_CLOSE

  12, // POS_OPEN
  13, // POS_EN
  14, // NC

  15, // HTR_EN
  16, // PWR_EN

  // function set pins
  17, // LED #1 (red)
  18, // SWO
  19, // LED #2 (blue)
  20, // DFU
  21, // Reset
  22, // Factory Reset
  23, // N/A
  24, // N/A

  25, // SDA - NC
  26, // SCL - NC
  27, // GPIO #27
  28, // A4 - NC
  29, // A5 - NC
  30, // A6 - THERM_EN
  31, // A7 - THERM_ADC
};

void initVariant()
{
  // LED1 & LED2
  pinMode(PIN_LED, OUTPUT);
  ledOff(PIN_LED);

  pinMode(PIN_LED2, OUTPUT);
  ledOff(PIN_LED2);

  pinMode(PIN_PWR_EN, OUTPUT);
  digitalWrite(PIN_PWR_EN, HIGH);   // Ensure PMIC stays powered ON

  pinMode(PIN_HTR_EN, OUTPUT);
  digitalWrite(PIN_HTR_EN, LOW);    // Ensure heater start off

  pinMode(PIN_MTR_CTL1, OUTPUT);
  pinMode(PIN_MTR_CTL2, OUTPUT);
  pinMode(PIN_MTR_EN, OUTPUT);
  digitalWrite(PIN_MTR_EN, LOW);    // Ensure motor start off

  pinMode(PIN_POS_EN, OUTPUT);
  digitalWrite(PIN_POS_EN, LOW);    // Ensure HAL sensors start off
  pinMode(PIN_POS_CLOSE, INPUT_PULLUP);
  pinMode(PIN_POS_OPEN, INPUT_PULLUP);

  pinMode(PIN_THERM_EN, OUTPUT);
  digitalWrite(PIN_THERM_EN, LOW);  // Ensure thermistor power starts off
}
