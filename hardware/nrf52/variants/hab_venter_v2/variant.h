 /*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_HAB_VENTER_
#define _VARIANT_HAB_VENTER_

/** Master clock frequency */
#define VARIANT_MCK       (64000000ul)

#define USE_LFXO      // Board uses 32khz crystal for LF
// define USE_LFRC    // Board uses RC for LF

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// Number of pins defined in PinDescription array
#define PINS_COUNT           (32u)
#define NUM_DIGITAL_PINS     (32u)
#define NUM_ANALOG_INPUTS    (8u)
#define NUM_ANALOG_OUTPUTS   (0u)

// LEDs
#define PIN_LED              (17)
#define PIN_LED2             (19)

#define LED_BUILTIN          PIN_LED
#define LED_CONN             PIN_LED2

#define LED_RED              PIN_LED
#define LED_BLUE             PIN_LED2

#define LED_STATE_ON         1         // State when LED is litted

/*
 * Analog pins
 */
#define PIN_A0               (2)
#define PIN_A1               (3)
#define PIN_A2               (4)
#define PIN_A3               (5)
#define PIN_A4               (28)
#define PIN_A5               (29)
#define PIN_A6               (30)
#define PIN_A7               (31)

static const uint8_t A0  = PIN_A0 ;
static const uint8_t A1  = PIN_A1 ;
static const uint8_t A2  = PIN_A2 ;
static const uint8_t A3  = PIN_A3 ;
static const uint8_t A4  = PIN_A4 ;
static const uint8_t A5  = PIN_A5 ;
static const uint8_t A6  = PIN_A6 ;
static const uint8_t A7  = PIN_A7 ;
#define ADC_RESOLUTION    14

// Other pins
#define PIN_AREF           (24)
#define PIN_VBAT           PIN_A3
// #define PIN_NFC1           (9)
// #define PIN_NFC2           (10)

static const uint8_t AREF = PIN_AREF;

/*
 * Serial interfaces
 */
#define PIN_SERIAL_RX       (8)
#define PIN_SERIAL_TX       (6)

/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 1

#define PIN_SPI_MISO         (14)
#define PIN_SPI_MOSI         (13)
#define PIN_SPI_SCK          (12)

static const uint8_t SS   = 24 ;
static const uint8_t MOSI = PIN_SPI_MOSI ;
static const uint8_t MISO = PIN_SPI_MISO ;
static const uint8_t SCK  = PIN_SPI_SCK ;

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 1

#define PIN_WIRE_SDA         (25u)
#define PIN_WIRE_SCL         (26u)

/*----------------------------------------------------------------------------
 *        Board specific defines
 *----------------------------------------------------------------------------*/
 #define HARDWARE_NAME			"HAB Venter"
 #define HARDWARE_VERSION 	(2)
 static const float HARDWARE_BATT_V_CONVERSION = 2.0; // Voltage divider ratio
 static const float SCAP_MV_PER_LSB = 2.0;

 /*-------- Hardware Subsystems ---------------------------------------------*/
 #define HWSS_VENTER
 #define HWSS_HEATER
 #define HWSS_AUX_CONN
 #define HWSS_STROBE
 #define HWSS_BURNWIRE
 #define HWSS_LORA			1262	//or 1272 for RFM95 modules

 // #define PIN_A0            (2)
 // #define PIN_A1            (3)
 #define PIN_CUTTER_EN      (4)
 // #define PIN_A3            (5)
 // #define PIN_UART_TX       (6)
 #define PIN_STRB_EN        (7)
 // #define PIN_UART_RX       (8)
 #define PIN_MTR_EN         (9)
 #define PIN_MTR_CTL1       (10)
 #define PIN_MTR_CTL2       (11)
 // #define PIN_SCK       (12)
 // #define PIN_COPI      (13)
 // #define PIN_CIPO      (14)
 #define PIN_HTR_EN         (15)
 #define PIN_PWR_EN         (16)
 // #define PIN_STATUS_LED     (17)
 #define PIN_HB_OUT         (18)
 // #define PIN_BLE_LED        (19)
 #define PIN_POS_CLOSE      (20)
 #define PIN_POS_OPEN       (22)
 #define PIN_AUX_0          (23)
 #define PIN_LORA_NSS       (24)
 // #define PIN_SDA            (25)
 // #define PIN_SCL            (26)
 #define PIN_LORA_IRQ       (27)
 #define PIN_LORA_RST       (28)
 #define PIN_LORA_BSY       (29)
 // #define PIN_A6       (30)
 // #define PIN_A7       (31)

 #define PIN_SCAP_V   	    PIN_A0
 #define PIN_CUTTER_CONT    PIN_A1
 //#define PIN_NC             PIN_A2
 #define PIN_BATT_V   	    PIN_A3
 // #define PIN_LIGHT_SENSE    PIN_A4
 // #define PIN_GPIO_1         PIN_A5
 #define PIN_LIGHT_SENSE   	PIN_A6
 // #define PIN_THERM_ADC      PIN_A7

#define NUM_CUTTERS			1											// How many cutters exist that can be enabled
#define PWM_TOP				19											// With 16 MHz clock / 19 counts => 842 kHz

#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#endif /* _VARIANT_HAB_VENTER_ */
