 /*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_ESTUARY_LOGGER_
#define _VARIANT_ESTUARY_LOGGER_

/** Master clock frequency */
#define VARIANT_MCK       (64000000ul)

#define USE_LFXO      // Board uses 32khz crystal for LF
// define USE_LFRC    // Board uses RC for LF

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// Number of pins defined in PinDescription array
#define PINS_COUNT           (32u)
#define NUM_DIGITAL_PINS     (32u)
#define NUM_ANALOG_INPUTS    (8u)
#define NUM_ANALOG_OUTPUTS   (0u)

// LEDs
#define PIN_LED              (17)
#define PIN_LED2             (19)

#define LED_BUILTIN          PIN_LED
#define LED_CONN             PIN_LED2

#define LED_RED              PIN_LED
#define LED_BLUE             PIN_LED2

#define LED_STATE_ON         1         // State when LED is litted

/*
 * Analog pins
 */
#define PIN_A0               (2)
#define PIN_A1               (3)
#define PIN_A2               (4)
#define PIN_A3               (5)
#define PIN_A4               (28)
#define PIN_A5               (29)
#define PIN_A6               (30)
#define PIN_A7               (31)

static const uint8_t A0  = PIN_A0 ;
static const uint8_t A1  = PIN_A1 ;
static const uint8_t A2  = PIN_A2 ;
static const uint8_t A3  = PIN_A3 ;
static const uint8_t A4  = PIN_A4 ;
static const uint8_t A5  = PIN_A5 ;
static const uint8_t A6  = PIN_A6 ;
static const uint8_t A7  = PIN_A7 ;
#define ADC_RESOLUTION    14

// Other pins
#define PIN_AREF           (24)
#define PIN_VBAT           PIN_A3
// #define PIN_NFC1           (9)
// #define PIN_NFC2           (10)

static const uint8_t AREF = PIN_AREF;

/*
 * Serial interfaces
 */
#define PIN_SERIAL_RX       (8)
#define PIN_SERIAL_TX       (6)

/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 1

#define PIN_SPI_MISO         (14)
#define PIN_SPI_MOSI         (13)
#define PIN_SPI_SCK          (12)

static const uint8_t SS   = 15 ;
static const uint8_t MOSI = PIN_SPI_MOSI ;
static const uint8_t MISO = PIN_SPI_MISO ;
static const uint8_t SCK  = PIN_SPI_SCK ;

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 1

#define PIN_WIRE_SDA         (25u)
#define PIN_WIRE_SCL         (26u)

/*----------------------------------------------------------------------------
 *        Board specific defines
 *----------------------------------------------------------------------------*/
 #define HARDWARE_NAME			"Estuary Logger"
 #define HARDWARE_VERSION 	(1)
 static const float  HARDWARE_BATT_V_CONVERSION = 6.1; // Voltage divider ratio

 /*-------- Hardware Subsystems ---------------------------------------------*/
 #define HWSS_AUX_CONN
 #define HWSS_SDCARD
 #define HWSS_UBLOX_GPS
 #define HWSS_MS5611
 #define HWSS_AJ_SR04M
 #define HWSS_PROFILER

 #define PIN_AUX_0          (2)
 #define PIN_MTR_CTL1       (3)
 #define PIN_MTR_CTL2       (4)
 #define PIN_MTR_EN         (7)
 #define PIN_LMSW_1         (9)
 #define PIN_SONDE_PWR      (10)
 #define PIN_LMSW_2         (11)
 #define PIN_SD_CS          (15)
 #define PIN_PWR_EN         (16)
 #define PIN_RS_D           (20)
 #define PIN_RS_R           (22)
 #define PIN_RS_DE          (23)
 #define PIN_RS_RE          (24)
 #define PIN_RANGE_RX       (27)
 #define PIN_RANGE_TX       (28)
 #define PIN_MTR_ENC_A      (29)
 #define PIN_MTR_ENC_B      (30)
 #define PIN_AUX_1          (31)

 // #define PIN_AUX_0   	      PIN_A0  // D02
 // #define PIN_CUTTER_0_CONT  PIN_A1  // D03
 // #define PIN_CUTTER_1_CONT  PIN_A2  // D04
 #define PIN_BATT_V   	    PIN_A3  // D05
 // #define PIN_BATT_V   	    PIN_A4  // D28
 // #define PIN_GPIO_1         PIN_A5  // D29
 // #define PIN_GPIO_1         PIN_A6  // D30
 // #define PIN_THERM_ADC      PIN_A7  // D31

#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#endif /* _VARIANT_HAB_BALLASTER_ */
