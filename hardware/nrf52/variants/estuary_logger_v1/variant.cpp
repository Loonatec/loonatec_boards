/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "variant.h"

#include "wiring_constants.h"
#include "wiring_digital.h"
#include "nrf.h"
#include "nrfx_pwm.h"

const uint32_t g_ADigitalPinMap[] = {
  // D0 - D7
  0,  // xtal 1
  1,  // xtal 2
  2,  // AUX_0
  3,  // MTR_IN1
  4,  // MTR_IN2
  5,  // A3 - Batt_Volt
  6,  // TXD
  7,  // MTR_EN

  // D8 - D13
  8,  // RXD

  9,  // LMSW_1
  10, // SONDE_PWR

  11, // LMSW_2

  12, // SCK
  13, // COPI
  14, // CIPO

  15, // SD_CS
  16, // PWR_EN

  // function set pins
  17, // LED #1 (red)
  18, // SWO
  19, // LED #2 (blue)
  20, // D
  21, // Reset
  22, // R
  23, // DE
  24, // ~RE

  25, // SDA
  26, // SCL
  27, // RANGE_RX
  28, // RANGE_TX
  29, // MTR_ENC_A
  30, // MTR_ENC_B
  31, // AUX_1
};

void initVariant() {
  pinMode(PIN_LED, OUTPUT);
    ledOff(PIN_LED);
  pinMode(PIN_LED2, OUTPUT);
    ledOff(PIN_LED2);

  pinMode(PIN_PWR_EN, OUTPUT);
    digitalWrite(PIN_PWR_EN, HIGH);   // Ensure PMIC stays powered ON
  pinMode(PIN_MTR_EN, OUTPUT);
    digitalWrite(PIN_MTR_EN, LOW);    // Ensure motor start off
  pinMode(PIN_SONDE_PWR, OUTPUT);
    digitalWrite(PIN_SONDE_PWR, LOW); // Ensure SONDE start off

  pinMode(PIN_MTR_CTL1, OUTPUT);
    digitalWrite(PIN_MTR_CTL1, LOW);    // Ensure motor start off
  pinMode(PIN_MTR_CTL2, OUTPUT);
    digitalWrite(PIN_MTR_CTL2, LOW);    // Ensure motor start off
  pinMode(PIN_MTR_ENC_A, INPUT_PULLUP);
  pinMode(PIN_MTR_ENC_B, INPUT_PULLUP);
  pinMode(PIN_LMSW_1, INPUT_PULLDOWN);
  pinMode(PIN_LMSW_2, INPUT_PULLDOWN);
  pinMode(PIN_AUX_0, INPUT);
  pinMode(PIN_AUX_1, INPUT);

}
