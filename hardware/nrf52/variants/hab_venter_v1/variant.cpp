/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "variant.h"

#include "wiring_constants.h"
#include "wiring_digital.h"
#include "nrf.h"
#include "nrfx_pwm.h"

const uint32_t g_ADigitalPinMap[] = {
  // D0 - D7
  0,  // xtal 1
  1,  // xtal 2
  2,  // PGOOD
  3,  // MTR_IN1
  4,  // MTR_IN2
  5,  // a3 - Batt_Volt
  6,  // TXD
  7,  // MTR_EN

  // D8 - D13
  8,  // RXD

  9,  // NFC1 - NC
  10, // NFC2 - NC

  11, // POS_CLOSE

  12, // POS_OPEN
  13, // POS_EN
  14, // NC

  15, // HTR_EN
  16, // PWR_EN

  // function set pins
  17, // LED #1 (red)
  18, // SWO
  19, // LED #2 (blue)
  20, // DFU
  21, // Reset
  22, // Factory Reset
  23, // N/A
  24, // N/A

  25, // SDA - NC
  26, // SCL - NC
  27, // GPIO #27
  28, // A4 - NC
  29, // A5 - NC
  30, // A6 - THERM_EN
  31, // A7 - THERM_ADC
};

/* Burn-Wire PWM Settings */
nrfx_pwm_t burnwire_pwm = NRFX_PWM_INSTANCE(0);				// 3 PWM modules to choose from on nRF52832, 4 on nRF52840
nrfx_pwm_config_t burnwire_pwm_config = {
	.output_pins = {
		PIN_PWR_EN, 										// channel_0 - Note: each PWM module can control up to 4 output channels
		NRFX_PWM_PIN_NOT_USED, 								// channel_1
		NRFX_PWM_PIN_NOT_USED, 								// channel_2
		NRFX_PWM_PIN_NOT_USED  								// channel_3
	},
	.irq_priority = 7,										// See above, APP_LOWEST = 7
	.base_clock   = NRF_PWM_CLK_16MHz,						// Fastest PWM clock available
	.count_mode   = NRF_PWM_MODE_UP,						// Fastest PWM mode
	.top_value    = PWM_TOP,								// With 16 MHz clock / 19 counts => 842 kHz
	.load_mode    = NRF_PWM_LOAD_COMMON,					// Common, only ever firing a single cutter at a time
	.step_mode    = NRF_PWM_STEP_AUTO						// AUTO, don't trigger a task every cycle
};
nrf_pwm_values_common_t burnwire_pwm_bottom = 10;			// Initial PWM count value at which to turn on at, off at TOP
nrf_pwm_sequence_t burnwire_pwm_sequence = {
    &burnwire_pwm_bottom,									// Note: if nrf_pwm_values is an array, then don't prepending a '&'
    NRF_PWM_VALUES_LENGTH(burnwire_pwm_bottom),				// Likely 1 for common mode, unless nrf_pwm_values is an array
	842105,													// 1 sec with 16 MHz and TOP=19 - # of repeats for each step of the array, not number of times array repeats
    0
};

void initVariant() {
  pinMode(PIN_CUTTER_EN, OUTPUT);      // Ensure Cutter is off
    digitalWrite(PIN_CUTTER_EN, LOW);
  pinMode(PIN_MTR_EN, OUTPUT);         // Ensure Motor is off
    digitalWrite(PIN_MTR_EN, LOW);
  pinMode(PIN_MTR_CTL1, OUTPUT);
    digitalWrite(PIN_MTR_CTL1, LOW);
    pinMode(PIN_MTR_CTL2, OUTPUT);
      digitalWrite(PIN_MTR_CTL2, LOW);

  // LED1 & LED2
  pinMode(PIN_LED, OUTPUT);
    ledOff(PIN_LED);
  pinMode(PIN_LED2, OUTPUT);
    ledOff(PIN_LED2);
  pinMode(PIN_STRB_EN, OUTPUT);			// Enable the strobe output, ensure off
  	digitalWrite(PIN_STRB_EN, LOW);

  pinMode(PIN_PWR_EN, OUTPUT);
    digitalWrite(PIN_PWR_EN, HIGH);    // Ensure PMIC stays powered ON

  pinMode(PIN_HTR_EN, OUTPUT);
    digitalWrite(PIN_HTR_EN, LOW);     // Ensure heater start off

  pinMode(PIN_THERM_EN, OUTPUT);
    digitalWrite(PIN_THERM_EN, LOW);   // Ensure thermistor power starts off

    pinMode(PIN_HB_OUT, OUTPUT);
      digitalWrite(PIN_HB_OUT, LOW);   // Ensure Heartbeat output starts off

  pinMode(PIN_EXT_BIAS, INPUT);        // Set to low-power inputs
  pinMode(PIN_EXT_TRIG, INPUT);
  pinMode(PIN_AUX_0, INPUT);
  pinMode(PIN_GPIO_1, INPUT);
  pinMode(PIN_POS_OPEN, INPUT);
  pinMode(PIN_POS_CLOSE, INPUT);
  pinMode(PIN_LIGHT_SENSE, INPUT);
  pinMode(PIN_SCAP_V, INPUT);
}
