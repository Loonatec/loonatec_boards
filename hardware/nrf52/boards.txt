# Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
# Copyright (c) 2016 Sandeep Mistry All right reserved.
# Copyright (c) 2017 Adafruit Industries.  All right reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

menu.softdevice=Bootloader
menu.debug=Debug

# ----------------------------------
# HAB Ballaster v1
# ----------------------------------
ballaster_v1.name=HAB Ballaster v1
ballaster_v1.bootloader.tool=bootburn

# Upload
ballaster_v1.upload.tool=nrfutil
ballaster_v1.upload.protocol=nrfutil
ballaster_v1.upload.use_1200bps_touch=false
ballaster_v1.upload.wait_for_upload_port=false
ballaster_v1.upload.native_usb=false
ballaster_v1.upload.maximum_size=290816
ballaster_v1.upload.maximum_data_size=52224

# Build
ballaster_v1.build.mcu=cortex-m4
ballaster_v1.build.f_cpu=64000000
ballaster_v1.build.board=HAB_Ballaster_v1
ballaster_v1.build.core=nRF5
ballaster_v1.build.variant=HAB_Ballaster_v1
ballaster_v1.build.usb_manufacturer="Loonatec"
ballaster_v1.build.usb_product="HAB Ballaster"
ballaster_v1.build.extra_flags=-DNRF52832_XXAA -DNRF52
ballaster_v1.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
ballaster_v1.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
ballaster_v1.menu.softdevice.s132v6.build.sd_name=s132
ballaster_v1.menu.softdevice.s132v6.build.sd_version=6.1.1
ballaster_v1.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
ballaster_v1.menu.debug.l0=Level 0 (Release)
ballaster_v1.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
ballaster_v1.menu.debug.l1=Level 1 (Error Message)
ballaster_v1.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
ballaster_v1.menu.debug.l2=Level 2 (Full Debug)
ballaster_v1.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
ballaster_v1.menu.debug.l3=Level 3 (Segger SystemView)
ballaster_v1.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
ballaster_v1.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Ballaster v2
# ----------------------------------
ballaster_v2.name=HAB Ballaster v2
ballaster_v2.bootloader.tool=bootburn

# Upload
ballaster_v2.upload.tool=nrfutil
ballaster_v2.upload.protocol=nrfutil
ballaster_v2.upload.use_1200bps_touch=false
ballaster_v2.upload.wait_for_upload_port=false
ballaster_v2.upload.native_usb=false
ballaster_v2.upload.maximum_size=290816
ballaster_v2.upload.maximum_data_size=52224

# Build
ballaster_v2.build.mcu=cortex-m4
ballaster_v2.build.f_cpu=64000000
ballaster_v2.build.board=HAB_Ballaster_v2
ballaster_v2.build.core=nRF5
ballaster_v2.build.variant=HAB_Ballaster_v2
ballaster_v2.build.usb_manufacturer="Loonatec"
ballaster_v2.build.usb_product="HAB Ballaster"
ballaster_v2.build.extra_flags=-DNRF52832_XXAA -DNRF52
ballaster_v2.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
ballaster_v2.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
ballaster_v2.menu.softdevice.s132v6.build.sd_name=s132
ballaster_v2.menu.softdevice.s132v6.build.sd_version=6.1.1
ballaster_v2.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
ballaster_v2.menu.debug.l0=Level 0 (Release)
ballaster_v2.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
ballaster_v2.menu.debug.l1=Level 1 (Error Message)
ballaster_v2.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
ballaster_v2.menu.debug.l2=Level 2 (Full Debug)
ballaster_v2.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
ballaster_v2.menu.debug.l3=Level 3 (Segger SystemView)
ballaster_v2.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
ballaster_v2.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Cutter v1
# ----------------------------------
cutter_v1.name=HAB Cutter v1
cutter_v1.bootloader.tool=bootburn

# Upload
cutter_v1.upload.tool=nrfutil
cutter_v1.upload.protocol=nrfutil
cutter_v1.upload.use_1200bps_touch=false
cutter_v1.upload.wait_for_upload_port=false
cutter_v1.upload.native_usb=false
cutter_v1.upload.maximum_size=290816
cutter_v1.upload.maximum_data_size=52224

# Build
cutter_v1.build.mcu=cortex-m4
cutter_v1.build.f_cpu=64000000
cutter_v1.build.board=HAB_Cutter_v1
cutter_v1.build.core=nRF5
cutter_v1.build.variant=HAB_Cutter_v1
cutter_v1.build.usb_manufacturer="Loonatec"
cutter_v1.build.usb_product="HAB Cutter"
cutter_v1.build.extra_flags=-DNRF52832_XXAA -DNRF52
cutter_v1.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
cutter_v1.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
cutter_v1.menu.softdevice.s132v6.build.sd_name=s132
cutter_v1.menu.softdevice.s132v6.build.sd_version=6.1.1
cutter_v1.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
cutter_v1.menu.debug.l0=Level 0 (Release)
cutter_v1.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
cutter_v1.menu.debug.l1=Level 1 (Error Message)
cutter_v1.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
cutter_v1.menu.debug.l2=Level 2 (Full Debug)
cutter_v1.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
cutter_v1.menu.debug.l3=Level 3 (Segger SystemView)
cutter_v1.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
cutter_v1.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Cutter v2
# ----------------------------------
cutter_v2.name=HAB Cutter v2
cutter_v2.bootloader.tool=bootburn

# Upload
cutter_v2.upload.tool=nrfutil
cutter_v2.upload.protocol=nrfutil
cutter_v2.upload.use_1200bps_touch=false
cutter_v2.upload.wait_for_upload_port=false
cutter_v2.upload.native_usb=false
cutter_v2.upload.maximum_size=290816
cutter_v2.upload.maximum_data_size=52224

# Build
cutter_v2.build.mcu=cortex-m4
cutter_v2.build.f_cpu=64000000
cutter_v2.build.board=HAB_Cutter_v2
cutter_v2.build.core=nRF5
cutter_v2.build.variant=HAB_Cutter_v2
cutter_v2.build.usb_manufacturer="Loonatec"
cutter_v2.build.usb_product="HAB Cutter"
cutter_v2.build.extra_flags=-DNRF52832_XXAA -DNRF52
cutter_v2.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
cutter_v2.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
cutter_v2.menu.softdevice.s132v6.build.sd_name=s132
cutter_v2.menu.softdevice.s132v6.build.sd_version=6.1.1
cutter_v2.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
cutter_v2.menu.debug.l0=Level 0 (Release)
cutter_v2.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
cutter_v2.menu.debug.l1=Level 1 (Error Message)
cutter_v2.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
cutter_v2.menu.debug.l2=Level 2 (Full Debug)
cutter_v2.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
cutter_v2.menu.debug.l3=Level 3 (Segger SystemView)
cutter_v2.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
cutter_v2.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Igniter v1
# ----------------------------------
igniter_v1.name=HAB Igniter v1
igniter_v1.bootloader.tool=bootburn

# Upload
igniter_v1.upload.tool=nrfutil
igniter_v1.upload.protocol=nrfutil
igniter_v1.upload.use_1200bps_touch=false
igniter_v1.upload.wait_for_upload_port=false
igniter_v1.upload.native_usb=false
igniter_v1.upload.maximum_size=290816
igniter_v1.upload.maximum_data_size=52224

# Build
igniter_v1.build.mcu=cortex-m4
igniter_v1.build.f_cpu=64000000
igniter_v1.build.board=HAB_Igniter_v1
igniter_v1.build.core=nRF5
igniter_v1.build.variant=HAB_Igniter_v1
igniter_v1.build.usb_manufacturer="Loonatec"
igniter_v1.build.usb_product="HAB Igniter"
igniter_v1.build.extra_flags=-DNRF52832_XXAA -DNRF52
igniter_v1.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
igniter_v1.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
igniter_v1.menu.softdevice.s132v6.build.sd_name=s132
igniter_v1.menu.softdevice.s132v6.build.sd_version=6.1.1
igniter_v1.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
igniter_v1.menu.debug.l0=Level 0 (Release)
igniter_v1.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
igniter_v1.menu.debug.l1=Level 1 (Error Message)
igniter_v1.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
igniter_v1.menu.debug.l2=Level 2 (Full Debug)
igniter_v1.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
igniter_v1.menu.debug.l3=Level 3 (Segger SystemView)
igniter_v1.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
igniter_v1.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Venter v1
# ----------------------------------
venter_v1.name=HAB Venter v1
venter_v1.bootloader.tool=bootburn

# Upload
venter_v1.upload.tool=nrfutil
venter_v1.upload.protocol=nrfutil
venter_v1.upload.use_1200bps_touch=false
venter_v1.upload.wait_for_upload_port=false
venter_v1.upload.native_usb=false
venter_v1.upload.maximum_size=290816
venter_v1.upload.maximum_data_size=52224

# Build
venter_v1.build.mcu=cortex-m4
venter_v1.build.f_cpu=64000000
venter_v1.build.board=HAB_Venter_v1
venter_v1.build.core=nRF5
venter_v1.build.variant=HAB_Venter_v1
venter_v1.build.usb_manufacturer="Loonatec"
venter_v1.build.usb_product="HAB Venter"
venter_v1.build.extra_flags=-DNRF52832_XXAA -DNRF52
venter_v1.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
venter_v1.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
venter_v1.menu.softdevice.s132v6.build.sd_name=s132
venter_v1.menu.softdevice.s132v6.build.sd_version=6.1.1
venter_v1.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
venter_v1.menu.debug.l0=Level 0 (Release)
venter_v1.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
venter_v1.menu.debug.l1=Level 1 (Error Message)
venter_v1.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
venter_v1.menu.debug.l2=Level 2 (Full Debug)
venter_v1.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
venter_v1.menu.debug.l3=Level 3 (Segger SystemView)
venter_v1.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
venter_v1.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# HAB Venter v2
# ----------------------------------
venter_v2.name=HAB Venter v2
venter_v2.bootloader.tool=bootburn

# Upload
venter_v2.upload.tool=nrfutil
venter_v2.upload.protocol=nrfutil
venter_v2.upload.use_1200bps_touch=false
venter_v2.upload.wait_for_upload_port=false
venter_v2.upload.native_usb=false
venter_v2.upload.maximum_size=290816
venter_v2.upload.maximum_data_size=52224

# Build
venter_v2.build.mcu=cortex-m4
venter_v2.build.f_cpu=64000000
venter_v2.build.board=HAB_Venter_v2
venter_v2.build.core=nRF5
venter_v2.build.variant=HAB_Venter_v2
venter_v2.build.usb_manufacturer="Loonatec"
venter_v2.build.usb_product="HAB Venter"
venter_v2.build.extra_flags=-DNRF52832_XXAA -DNRF52
venter_v2.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
venter_v2.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
venter_v2.menu.softdevice.s132v6.build.sd_name=s132
venter_v2.menu.softdevice.s132v6.build.sd_version=6.1.1
venter_v2.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
venter_v2.menu.debug.l0=Level 0 (Release)
venter_v2.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
venter_v2.menu.debug.l1=Level 1 (Error Message)
venter_v2.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
venter_v2.menu.debug.l2=Level 2 (Full Debug)
venter_v2.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
venter_v2.menu.debug.l3=Level 3 (Segger SystemView)
venter_v2.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
venter_v2.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1

# ----------------------------------
# Estuary Logger v1
# ----------------------------------
estuary_logger_v1.name=Estuary Logger v1
estuary_logger_v1.bootloader.tool=bootburn

# Upload
estuary_logger_v1.upload.tool=nrfutil
estuary_logger_v1.upload.protocol=nrfutil
estuary_logger_v1.upload.use_1200bps_touch=false
estuary_logger_v1.upload.wait_for_upload_port=false
estuary_logger_v1.upload.native_usb=false
estuary_logger_v1.upload.maximum_size=290816
estuary_logger_v1.upload.maximum_data_size=52224

# Build
estuary_logger_v1.build.mcu=cortex-m4
estuary_logger_v1.build.f_cpu=64000000
estuary_logger_v1.build.board=estuary_logger_v1
estuary_logger_v1.build.core=nRF5
estuary_logger_v1.build.variant=estuary_logger_v1
estuary_logger_v1.build.usb_manufacturer="Loonatec"
estuary_logger_v1.build.usb_product="Estuary Logger"
estuary_logger_v1.build.extra_flags=-DNRF52832_XXAA -DNRF52
estuary_logger_v1.build.ldscript=nrf52832_s132_v6.ld

# SofDevice Menu
estuary_logger_v1.menu.softdevice.s132v6=0.3.2 SoftDevice s132 6.1.1
estuary_logger_v1.menu.softdevice.s132v6.build.sd_name=s132
estuary_logger_v1.menu.softdevice.s132v6.build.sd_version=6.1.1
estuary_logger_v1.menu.softdevice.s132v6.build.sd_fwid=0x00B7

# Debug Menu
estuary_logger_v1.menu.debug.l0=Level 0 (Release)
estuary_logger_v1.menu.debug.l0.build.debug_flags=-DCFG_DEBUG=0
estuary_logger_v1.menu.debug.l1=Level 1 (Error Message)
estuary_logger_v1.menu.debug.l1.build.debug_flags=-DCFG_DEBUG=1
estuary_logger_v1.menu.debug.l2=Level 2 (Full Debug)
estuary_logger_v1.menu.debug.l2.build.debug_flags=-DCFG_DEBUG=2
estuary_logger_v1.menu.debug.l3=Level 3 (Segger SystemView)
estuary_logger_v1.menu.debug.l3.build.debug_flags=-DCFG_DEBUG=3
estuary_logger_v1.menu.debug.l3.build.sysview_flags=-DCFG_SYSVIEW=1
