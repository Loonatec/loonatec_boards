#######################################
# Syntax Coloring Map For Loonatec nRF52 Boards
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

TimeoutTimer	KEYWORD1
Adafruit_FIFO	KEYWORD1
HardwarePWM	KEYWORD1

#######################################
# Core Function (KEYWORD2)
#######################################

digitalToggle	KEYWORD2
waitForEvent	KEYWORD2
systemOff	KEYWORD2
suspendLoop	KEYWORD2

enterSerialDfu	KEYWORD2
enterOTADfu	KEYWORD2

ledOn	KEYWORD2
ledOff	KEYWORD2

printf	KEYWORD2
printBuffer	KEYWORD2
printBufferReverse	KEYWORD2

dwt_enable	KEYWORD2
dwt_disable	KEYWORD2
delay_ns	KEYWORD2

# RTOS
rtos_malloc	KEYWORD2
rtos_free	KEYWORD2

nrf52_flash_init	KEYWORD2
nrf52_flash_erase_sector	KEYWORD2
nrf52_flash_write	KEYWORD2

VERIFY_STATIC	KEYWORD2
VERIFY_STATUS	KEYWORD2
VERIFY	KEYWORD2

dbgHeapTotal	KEYWORD2
dbgHeapUsed	KEYWORD2
dbgHeapFree	KEYWORD2
dbgMemInfo	KEYWORD2
dbgPrintVersion	KEYWORD2

startLoop	KEYWORD2
setPins	KEYWORD2


#######################################
# Adafruit_FIFO Methods (KEYWORD2)
#######################################

clear	KEYWORD2
peek	KEYWORD2
peekAt	KEYWORD2
empty	KEYWORD2
full	KEYWORD2
count	KEYWORD2
remaining	KEYWORD2
remaining	KEYWORD2	KEYWORD2

#######################################
# TimeoutTimer Methods (KEYWORD2)
#######################################

set	KEYWORD2
expired	KEYWORD2
restart	KEYWORD2
reset	KEYWORD2

#######################################
# HardwarePWM Methods (KEYWORD2)
#######################################

setResolution	KEYWORD2
setClockDiv	KEYWORD2
addPin	KEYWORD2
pin2channel	KEYWORD2

checkPin	KEYWORD2

enabled	KEYWORD2
writePin	KEYWORD2
writeChannel	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################
INPUT_PULLDOWN	LITERAL1
INPUT_PULLUP	LITERAL1
INPUT_PULLUP_SENSE	LITERAL1
INPUT_PULLDOWN_SENSE	LITERAL1

PIN_A0	LITERAL1
PIN_A1	LITERAL1
PIN_A2	LITERAL1
PIN_A3	LITERAL1
PIN_A4	LITERAL1
PIN_A5	LITERAL1
PIN_A6	LITERAL1
PIN_A7	LITERAL1

PIN_AREF	LITERAL1
PIN_LED LITERAL1

ISR_DEFERRED	LITERAL1

INPUT_PULLDOWN  LITERAL1


######################################
# Board Specific Constants (LITERAL1)
#######################################
HARDWARE_NAME LITERAL1
HARDWARE_VERSION  LITERAL1

PIN_PWR_OK  LITERAL1
PIN_MTR_CTL1  LITERAL1
PIN_MTR_CTL2  LITERAL1
PIN_MTR_EN  LITERAL1
PIN_POS_CLOSE  LITERAL1
PIN_POS_OPEN  LITERAL1
PIN_POS_EN  LITERAL1
PIN_HTR_EN  LITERAL1
PIN_PWR_EN  LITERAL1
PIN_THERM_EN  LITERAL1
PIN_THERM_ADC  LITERAL1

PIN_CUTTER_EN  LITERAL1
PIN_CUTTER_0  LITERAL1
PIN_CUTTER_1  LITERAL1
PIN_CUTTER_0_CONT  LITERAL1
PIN_CUTTER_1_CONT  LITERAL1
PIN_EXT_BIAS  LITERAL1
PIN_EXT_TRIG  LITERAL1
PIN_AUX_0   LITERAL1
PIN_SCAP_V  LITERAL1
PIN_BATT_V   LITERAL1
PIN_GPIO_1   LITERAL1

PIN_STRB_EN   LITERAL1
PIN_HB_OUT    LITERAL1
PIN_LIGHT_SENSE LITERAL1
